package com.androidexample.customspinner;


public class SpinnerModel {
	
	private  String Name="";
	private  String Image=""; 
	private  String Id="";

	
	/*********** Set Methods ******************/
	public void setPersonName(String PersonName)
	{
		this.Name = PersonName;
	}
	
	public void setImage(String Image)
	{
		this.Image = Image;
	}
	
	public void setId(String Id)
	{
		this.Id = Id;
	}
	
	
	
	
	/*********** Get Methods ****************/
	public String getPersonName()
	{
		return this.Name;
	}
	
	public String getImage()
	{
		return this.Image;
	}

	public String getId()
	{
		return this.Id;
	}	

}
